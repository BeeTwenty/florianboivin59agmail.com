from random import *
 
def choix_utilisateur():
   """Fonction permettant de faire son choix"""
    joueur = input("Pierre\nPapier\nCiseaux\n")
    while joueur not in ["pierre","papier","ciseaux"]:
        joueur = input("pierre\npapier\nciseaux\n")
    return(joueur)
 
def choix_ordinateur():
    """Fonction permettant à l'ordinateur de générer aléatoirement un mouvement"""
    n = randint(1,3)
    """cette fonction permet de génerer de manière alétoire un entier entre 1 et 3 représentant l'un des 3 résultat du pierre feuille ciseau"""
    if n == 1:
        ordi = "pierre"
    elif n==2:
        """continuation de la boucle"""
        ordi = "papier"
    else:
        ordi = "ciseaux"
    return(ordi)
 
a = choix_utilisateur()
b = choix_ordinateur()
print(b)
"""sélection du 2eme joueur et définition du résultat du match pour chaque option"""
if a == "pierre" and b == "ciseaux":
    print("Vous avez gagné.")
elif a == "pierre" and b == "papier":
    print("Vous avez perdu.")
elif a == "pierre" and b == "pierre":
    print("Egalite.")
 
if a == "papier" and b == "pierre":
    print("Vous avez gagne.")
elif a == "papier" and b == "ciseaux":
    print("Vous avez perdu.")
elif a == "papier" and b == "papier":
    print("Egalite.")
 
if a == "ciseaux" and b == "papier":
    print("Vous avez gagne.")
elif a == "ciseaux" and b == "pierre":
    print("Vous avez perdu.")
elif a == "ciseaux" and b == "ciseaux":
    print("Egalite.")
